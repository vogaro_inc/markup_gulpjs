
# 1_環境構築

[TOC]


________________________________________
****************************************
## Gulp.jsとは

URL: [http://gulpjs.com/](http://gulpjs.com/)


Node.js上で動作するタスク自動化するフロントエンド開発者向けビルドツール。  
普段手作業で行っていた部分をタスクの自動化で、作業効率化やヒューマンエラーを未然に防いだりすることが出来ます。  
代表的なGruntJSもあるが、GruntJSよりコードがシンプルで高速に動作する。  
他にもbower, yeomen, webpack等がある。


### 代表的な作業タスクとして
- ファイルコピー
- 画像圧縮
- CSSのベンダープレフィックス調整
- SASSコンパイル（CSSメタ言語のコンパイル）
- JSLint（JSコード品質チェック）
- ファイル連結・圧縮
- BrowserSync（ブラウザの同期）

### 他にもタスクいろいろ  
- 文字コード変換 UTF-8 → SHIFT-JIS
- HTML、XML、JSON等ファイル生成
- TypeScriptコンパイル
- ファイルリネーム
- リファレンス生成
などなど



________________________________________
****************************************

## Node.jsインストール
URL: [https://nodejs.org/](https://nodejs.org/)

Node.js上で動作するので、Node.jsをはじめにインストールする。


### Node.jsの2種類のインストール方法
1. NodeインストーラーDLしてインストール（ライトユーザー）
2. Nodeバージョン管理nodistのインストール（開発者向き）


#### 1. NodeインストーラーDLしてインストール（ライトユーザー）
特にバージョン気にせず使う人向きなので、Node.js自体のバージョン管理はできません。  
インストーラーをDLしてインストールするのみ。  
URL: [Node.js](https://nodejs.org/)  


#### 2. Nodeバージョン管理nodistのインストール（開発者向き）
開発環境にあわせてバージョンコントロールしたい人は以下参考にインストールしてください。

##### nodistのインストール手順参考（for windows）
- [node.jsを複数バージョン切り替えられるようにインストール（Mac:nodebrew Windows:nodist```）](http://qiita.com/Kackey/items/b41b11bcf1c0b0d76149)
- [WindowsでNode.js(npm)触るならnodistを使うといいかもしれない](http://nantokaworks.com/p1039/)
- [環境変数の設定方法](http://next.matrix.jp/config-path-win7.html)


### インストール後、コマンドでバージョン確認
[スクリーンショット](http://prntscr.com/72uzkz)
```
node -v
```

#### インストール後、コマンドでバージョン確認出来ない場合
- [環境変数の設定確認](http://ics-web.jp/lab/archives/3290#post_2)
- PC再起動してみる。
- アンインストールして、再インストールする。


________________________________________
****************************************

## npmインストール (Node Package Manager)

URL: [https://www.npmjs.com/](https://www.npmjs.com/)  
Node Package Manager の略。  
Nodeで作られたパッケージモジュールを管理するためのツール

### npmインストールコマンド
※ 古いとエラーが出る場合があるので念のため最新バージョンをインストールしておく
```
npm install -g npm
```

### package.json生成コマンド
npmを管理用のpackage.jsonを生成しておきます。  
コマンド後、enterで進んでもらって問題ないです。  
生成したpackage.jsonは後で編集可能です。

```
npm init
```

### インストール後、コマンドでバージョン確認
[スクリーンショット](http://prntscr.com/72uzxm)
```
npm -v
```



________________________________________
****************************************

## Gulp.jsインストール
Gulp.jsをグローバル・ローカルの両方インストールする。


#### グローバルインストールコマンド
```
npm install -g gulp
```

#### ローカルインストールコマンド
ローカルインストールは、任意のディレクトリでかまいませんが、今回はC直下にインストールします。  
ローカルインストールしたデータは、node_modulesフォルダが設置されてます。
```
npm install gulp --save-dev
```

#### インストールバージョンを確認
グローバル、ローカル共に同じバージョンか確認する。  
[スクリーンショット](http://prntscr.com/72uxbe)
```
gulp -v
```


________________________________________
****************************************
## Gulpプラグインインストール

タスク管理したいGulpプラグインをインストールする  
以下、プラグインリスト参考。  

- [http://gulpjs.com/plugins/](gulpjs)
- [https://www.npmjs.com/search?q=gulp](npm)



### インストール方法
1. 単体インストール
2. 一括インストール


### 1. 単体インストール
使用するプラグインを個別でインストールしたい場合はこちらを使用します。  

インストールプラグイン例：[gulp-pleeease cssプレフィックスの調整](https://www.npmjs.com/package/gulp-pleeease)

#### ローカルインストールコマンド
ローカルインストールディレクトリに移動してインストール。
```
npm install gulp-pleeease --save-dev
```

バージョン指定する場合は、以下参考
```
npm install プラグイン名@バージョン --save-dev
```


### 2. 一括インストール packarg.json
npmで生成したpackage.jsonを利用します。  

#### package.json記述例
devDependencies内に、プラグイン名と、バーションを記述する。  
最新バーションを指定する場合は、バージョン指定を*（アスタリスク）にする。

```
{
  "name": "glup",
  "version": "0.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "Vogaro Inc.",
  "license": "ISC",
  "devDependencies": {
    "browser-sync": "~1.8.3", // ブラウザ同期
    "gulp": "~3.8.11", // gulp（必要なければ削除）
    "gulp-imagemin": "~2.1.0", // 画像圧縮
    "gulp-pleeease": "~1.1.0", // cssベンダープレフィックス
    "gulp-jslint": "~1.6.4" // jsコードチェック
    "jshint-stylish": "*" // jsコードエラーログ出力
    "gulp-plumber": "*" // エラー時の強制終了回避
  }
}
```

#### インストールコマンド
package.jsonをインストールするディレクトリに配置して（今回はc直下）して以下コマンド
```
npm install
```


### インストールコマンド終了
インストールコマンド終了したらnode_modulesフォルダ内にプラグインがインストールされている。  
[スクリーンショット](http://prntscr.com/72vmvh)


### アンインストールコマンド
node_modulesフォルダで以下コマンド
```
npm uninstall プラグイン名
```



________________________________________
****************************************
## Gulpタスク設定


### サンプル作業ディレクトリ構造

sample_html  
  ∟ gulp  
  ∟ develop  
  ∟ src  
  ∟ httpdocs

#### gulp ディレクトリ
gulpfile.js(gulp実行)ファイルやタスクファイルを保管します。

#### develop ディレクトリ
開発コードを置き、ここのディレクトリのファイルのみで作業を行いアップロードは行いません。

#### src ディレクトリ
サイトで使用する編集を加えないライブラリコードを保管します。  
外部ライブラリなどDLしてきたコードも対象となります。

#### httpdocs ディレクトリ
公開用コードがここのディレクトリに生成されます。  
ここのファイルは、編集は加えてはいけません。


### サンプルgulpfile.js設定

```
/*--------------------------------------------------------------------------
  CONFIG
--------------------------------------------------------------------------*/

/**
 * PATH: フォルダパス設定
 */
var PATH = {
  develop : '../develop/',  // 開発用
  httpdocs: '../httpdocs/', // 公開用
  src     : '../src/'      // ライブラリソース用
};


/**
 * MODULE: 読み込むモジュール
 */
var MODULE = {
  gulp         : require('gulp'),
  browserSync  : require('browser-sync'),
  imagemin     : require('gulp-imagemin'),
  jshint       : require('gulp-jshint'),
  jshintStylish: require('jshint-stylish'),
  pleeease     : require('gulp-pleeease'),
  plumber      : require('gulp-plumber'),
  watch        : require('gulp-watch')
};


/**
 * tasks: デフォルトタスクリスト設定
 */
var tasks = ['watch', 'browserSync', 'imagemin', 'css', 'js', 'copy'];


/**
 * copy： copyタスク対象ファイル一覧
 */
var copy = [
  '*.md',
  '*.txt',
  '*.htm',
  '*.html',
  '*.php',
  '*.inc',
  '*.xml',
  '*.json',
  '*.ico',
  '*.swf',
  '*.pdf',
  '*.mp3',
  '*.mp4',
  '*.ogv',
  '*.webm',
  '*.zip',
  '.htaccess'
];


/**
 * imgs: imageminタスクの画像対象ファイル一覧
 */
var imgs = [
  PATH.develop + '**/*.jpg',
  PATH.develop + '**/*.jpeg',
  PATH.develop + '**/*.png',
  PATH.develop + '**/*.gif',
  PATH.develop + '**/*.svg'
];


/**
 * syncFiles: browserSync監視ファイル一覧
 */
var syncFiles = [
  PATH.httpdocs + '**/*.html',
  PATH.httpdocs + '**/*.inc',
  PATH.httpdocs + '**/*.php',
  PATH.httpdocs + '**/*.css',
  PATH.httpdocs + '**/*.js'
];


/**
 * pleeeaseOptions：pleeeaseオプション（cssベンダープレフィックス調整）
 */
var pleeeaseOptions = {
  browsers  : ['last 2 version', 'ie 8', 'ie 9', 'Android 2.3'],
  sourcemaps: false,
  mqpacker  : false,
  minifier  : false
};



/*--------------------------------------------------------------------------
  Task
--------------------------------------------------------------------------*/

/**
 * default: デフォルトタスク
 * cmd    : gulp
 */
MODULE.gulp.task('default', tasks);


/**
 * watch: ファイル監視
 * ※ デフォルトタスク実行で監視を開始します
 */
MODULE.gulp.task('watch', function(){
  // browserSync: 対象ファイルに変更が入れば、ブラウザをリロードします
  MODULE.gulp.watch(syncFiles)
  .on('change', function(file){
    MODULE.browserSync.reload();
  });

  // imagemin: 対象画像が変更・追加されれば自動で圧縮
  MODULE.gulp.watch(imgs, ['imagemin']);

  // copy：copy対象ファイルが変更・追加されれば自動でコピーを生成
  for(var j = 0; j < copy.length; j += 1){
    MODULE.gulp.watch(PATH.develop + '**/' + copy[j], ['copy']);
  }

  // css: ベンダープレフィックス調整をしてコピーを生成
  MODULE.gulp.watch(PATH.develop + '**/*.css', ['css']);

  // js: 対象jsファイルが変更・追加されれば自動的にjshintチェックしコピーを生成
  MODULE.gulp.watch(PATH.develop + '**/*.js', ['js']);
});


/**
 * browserSync: ブラウザ同期
 */
MODULE.gulp.task('browserSync', function(){
  MODULE.browserSync.init(null,{
   server: {
      baseDir: PATH.httpdocs
    }
  });
});


/**
 * copy: ファイルコピー
 */
MODULE.gulp.task('copy', function(){
  for(var i = 0; i < copy.length; i += 1){
    MODULE.gulp.src(PATH.develop + '**/' + copy[i])
    .pipe(MODULE.gulp.dest(PATH.httpdocs));
  }
});


/**
 * imagemin: 画像圧縮
 */
MODULE.gulp.task('imagemin', function(){
  MODULE.gulp.src(PATH.develop + '**/*.+(jpg|jpeg|png|gif|svg)')
  .pipe(MODULE.plumber())
  .pipe(MODULE.imagemin())
  .pipe(MODULE.gulp.dest(PATH.httpdocs));
});


/**
 * css: cssプレフィックス & コピー
 */
MODULE.gulp.task('css', function(){
  MODULE.gulp.src(PATH.develop + '**/*.css')
  .pipe(MODULE.plumber())
  .pipe(MODULE.pleeease(pleeeaseOptions))
  .pipe(MODULE.gulp.dest(PATH.httpdocs));
});


/**
 * js: jsHint & コピー
 */
MODULE.gulp.task('js', function(){
  // copy
  MODULE.gulp.src(PATH.src + 'js/**/*.js')
  .pipe(MODULE.plumber())
  .pipe(MODULE.gulp.dest(PATH.httpdocs));

  // lint & copy
  MODULE.gulp.src(PATH.develop + '**/*.js')
  .pipe(MODULE.plumber())
  .pipe(MODULE.jshint())
  .pipe(MODULE.jshint.reporter('jshint-stylish'))
  // .pipe(MODULE.header(LISENCE))
  .pipe(MODULE.gulp.dest(PATH.httpdocs));
});

```


### gulp実行
gulpフォルダでコマンドを立ち上げてコマンド

```
gulp
```
#### 実行内容
httpdocsに生成されたファイルを確認してもらえれば以下の内容が実行されています。  

- cssベンダープレフィックス調整
- 画像の圧縮
- ブラウザ同期
- jsコードチェック
- ファイルコピー



### gulp終了
コマンド画面を閉じればタスクを終了するか、
ctr + c 押したあと、y enterでタスクを終了する。






________________________________________
****************************************

## 参考サイト

### Node.js概要参考
- [Node.jsとは何か](https://app.codegrid.net/entry/nodejs-1)
- [リアルタイムWEBを実現する技術」の触りを知っておく為のまとめ](http://qiita.com/otmb/items/138f12c8832f78b2534d)


### Gulp
- [5分で導入できる！ タスクランナーGulpを使ってWeb制作を爆速にしよう](http://ics-web.jp/lab/archives/3290)
- [新鋭フロンエンド・タスクランナーツール を試してみました](http://dev.classmethod.jp/tool/gulpjs-part-001/)
- [gulpのアプローチ なぜグローバルとローカルにインストールが必要なのか](http://nakajmg.github.io/blog/2014-09-03/gulp-structure.html)
