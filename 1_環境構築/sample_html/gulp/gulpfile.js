/*--------------------------------------------------------------------------
	CONFIG
--------------------------------------------------------------------------*/


/**
 * PATH: フォルダパス設定
 */
var PATH = {
	develop : '../develop/',  // 開発用
	httpdocs: '../httpdocs/', // 公開用
	src     : '../src/'      // ライブラリソース用
};


/**
 * MODULE: 読み込むモジュール
 */
var MODULE = {
	gulp         : require('gulp'),
	browserSync  : require('browser-sync'),
	imagemin     : require('gulp-imagemin'),
	jshint       : require('gulp-jshint'),
	jshintStylish: require('jshint-stylish'),
	pleeease     : require('gulp-pleeease'),
	plumber      : require('gulp-plumber'),
	watch        : require('gulp-watch')
};


/**
 * tasks: デフォルトタスクリスト設定
 */
var tasks = ['watch', 'browserSync', 'imagemin', 'css', 'js', 'copy'];


/**
 * copy： copyタスク対象ファイル一覧
 */
var copy = [
	'*.md',
	'*.txt',
	'*.htm',
	'*.html',
	'*.php',
	'*.inc',
	'*.xml',
	'*.json',
	'*.ico',
	'*.swf',
	'*.pdf',
	'*.mp3',
	'*.mp4',
	'*.ogv',
	'*.webm',
	'*.zip',
	'.htaccess'
];


/**
 * imgs: imageminタスクの画像対象ファイル一覧
 */
var imgs = [
	PATH.develop + '**/*.jpg',
	PATH.develop + '**/*.jpeg',
	PATH.develop + '**/*.png',
	PATH.develop + '**/*.gif',
	PATH.develop + '**/*.svg'
];


/**
 * syncFiles: browserSync監視ファイル一覧
 */
var syncFiles = [
	PATH.httpdocs + '**/*.html',
	PATH.httpdocs + '**/*.inc',
	PATH.httpdocs + '**/*.php',
	PATH.httpdocs + '**/*.css',
	PATH.httpdocs + '**/*.js'
];


/**
 * pleeeaseOptions：pleeeaseオプション（cssベンダープレフィックス調整）
 */
var pleeeaseOptions = {
	browsers  : ['last 2 version', 'ie 8', 'ie 9', 'Android 4'],
	sourcemaps: false,
	mqpacker  : false,
	minifier  : false
};



/*--------------------------------------------------------------------------
	Task
--------------------------------------------------------------------------*/

/**
 * default: デフォルトタスク
 * cmd    : gulp
 */
MODULE.gulp.task('default', tasks);


/**
 * watch: ファイル監視
 * ※ デフォルトタスク実行で監視を開始します
 */
MODULE.gulp.task('watch', function(){
	// browserSync: 対象ファイルに変更が入れば、ブラウザをリロードします
	MODULE.gulp.watch(syncFiles)
	.on('change', function(file){
		MODULE.browserSync.reload();
	});

	// imagemin: 対象画像が変更・追加されれば自動で圧縮
	MODULE.gulp.watch(imgs, ['imagemin']);

	// copy：copy対象ファイルが変更・追加されれば自動でコピーを生成
	for(var j = 0; j < copy.length; j += 1){
		MODULE.gulp.watch(PATH.develop + '**/' + copy[j], ['copy']);
	}

	// css: ベンダープレフィックス調整をしてコピーを生成
	MODULE.gulp.watch(PATH.develop + '**/*.css', ['css']);

	// js: 対象jsファイルが変更・追加されれば自動的にjshintチェックしコピーを生成
	MODULE.gulp.watch(PATH.develop + '**/*.js', ['js']);
});



/**
 * browserSync: ブラウザ同期
 */
MODULE.gulp.task('browserSync', function(){
	MODULE.browserSync.init(null,{
	 server: {
    	baseDir: PATH.httpdocs
    }
  });
});


/**
 * copy: ファイルコピー
 */
MODULE.gulp.task('copy', function(){
	for(var i = 0; i < copy.length; i += 1){
		MODULE.gulp.src(PATH.develop + '**/' + copy[i])
		.pipe(MODULE.gulp.dest(PATH.httpdocs));
	}
});


/**
 * imagemin: 画像圧縮
 */
MODULE.gulp.task('imagemin', function(){
	MODULE.gulp.src(PATH.develop + '**/*.+(jpg|jpeg|png|gif|svg)')
	.pipe(MODULE.plumber())
	.pipe(MODULE.imagemin())
	.pipe(MODULE.gulp.dest(PATH.httpdocs));
});


/**
 * css: cssプレフィックス & コピー
 */
MODULE.gulp.task('css', function(){
	MODULE.gulp.src(PATH.develop + '**/*.css')
	.pipe(MODULE.plumber())
	.pipe(MODULE.pleeease(pleeeaseOptions))
	.pipe(MODULE.gulp.dest(PATH.httpdocs));
});


/**
 * js: jsHint & コピー
 */
MODULE.gulp.task('js', function(){
	// copy
	MODULE.gulp.src(PATH.src + 'js/**/*.js')
	.pipe(MODULE.plumber())
	.pipe(MODULE.gulp.dest(PATH.httpdocs));

	// lint & copy
	MODULE.gulp.src(PATH.develop + '**/*.js')
	.pipe(MODULE.plumber())
	.pipe(MODULE.jshint())
	.pipe(MODULE.jshint.reporter('jshint-stylish'))
	// .pipe(MODULE.header(LISENCE))
	.pipe(MODULE.gulp.dest(PATH.httpdocs));
});
